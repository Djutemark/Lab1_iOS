//
//  ViewController.swift
//  Hello Single View
//
//  Created by Dennis Jutemark on 2017-10-19.
//  Copyright © 2017 Dennis Jutemark. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var arr =  [
        "Hello",
        "It's me...",
        "I was wondering if after all the years you'd like to meet..."
    ]
    var index = 0
    
    @IBAction func buttonTap(_ sender: Any) {
        print(arr[index])
        index += 1
        index %= arr.count
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

